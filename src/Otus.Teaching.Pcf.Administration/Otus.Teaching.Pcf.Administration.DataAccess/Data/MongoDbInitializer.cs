﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private MongoClient _client;

        public MongoDbInitializer(MongoClient client)
            => _client = client;

        public async void InitializeDb()
        {
            IMongoDatabase database = _client.GetDatabase("promocodefactory"); //создаем БД если она отсутствует

            try
            {
                await database.DropCollectionAsync("employees");
                await database.CreateCollectionAsync("employees"); //создаем коллекцию сотрудников

                await database.DropCollectionAsync("roles");
                await database.CreateCollectionAsync("roles"); //создаем коллекцию ролей
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message); //если она уже существует то ловим ошибку
            }
            //если коллекции создались без ошибки то заполняем их
            IMongoCollection<Employee> employees = database.GetCollection<Employee>("employees"); 
            IMongoCollection<Role> roles = database.GetCollection<Role>("roles");
                               
            await roles.InsertManyAsync(FakeDataFactory.Roles);
            await employees.InsertManyAsync(FakeDataFactory.Employees);
        }

    }
}
