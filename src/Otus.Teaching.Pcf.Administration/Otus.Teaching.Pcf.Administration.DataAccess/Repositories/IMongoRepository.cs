﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using Otus.Teaching.Pcf.Administration.Core.Domain;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public interface IMongoRepository
        
    {
        Task<IEnumerable<BsonDocument>> GetAllAsync();
        
        Task<BsonDocument> GetByIdAsync(Guid id);
        
        Task<IEnumerable<BsonDocument>> GetRangeByIdsAsync(List<Guid> ids);
        
        Task<BsonDocument> GetFirstWhere(Expression<Func<BsonDocument, bool>> predicate);
        
        Task<IEnumerable<BsonDocument>> GetWhere(Expression<Func<BsonDocument, bool>> predicate);

        Task AddAsync(BsonDocument entity);

        Task UpdateAsync(BsonDocument entity);

        Task DeleteAsync(BsonDocument entity);
    }
}