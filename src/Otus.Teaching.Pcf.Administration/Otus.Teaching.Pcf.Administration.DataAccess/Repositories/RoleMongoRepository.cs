﻿using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class RoleMongoRepository : IRepository<Role>
    {
        private readonly MongoClient _client;

        private readonly IMongoDatabase _database;

        private IMongoCollection<Role> _roleCollection;

        public RoleMongoRepository(MongoClient client)
        {
            _client = client;
            _database = _client.GetDatabase("promocodefactory");
            _roleCollection = _database.GetCollection<Role>("roles");
            
        }

        public Task AddAsync(Role entity)
        {
            return _roleCollection.InsertOneAsync(entity);
        }

        public Task DeleteAsync(Role entity)
        {
            return _roleCollection.DeleteOneAsync(entity.ToJson());
        }

        public async Task<IEnumerable<Role>> GetAllAsync()
        {
            return await _roleCollection.Find("{}").ToListAsync();
        }

        public async Task<Role> GetByIdAsync(Guid id)
        {
            var filter = new BsonDocument { { "_id", $"{id}" } };
            return await _roleCollection.Find(filter).FirstAsync();
        }

        public async Task<Role> GetFirstWhere(Expression<Func<Role, bool>> predicate)
        {
            return await _roleCollection.Find(predicate).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Role>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await _roleCollection.Find(x => ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<IEnumerable<Role>> GetWhere(Expression<Func<Role, bool>> predicate)
        {
            return await _roleCollection.Find(predicate).ToListAsync();
        }

        public Task UpdateAsync(Role entity)
        {
            var filter = new BsonDocument { { "_id", $"{entity.Id}" } };
            return _roleCollection.ReplaceOneAsync(filter, entity);
        }
    }
}
